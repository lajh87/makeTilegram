# makeTilegram 0.1

* Submitted for CRAN on 11/02/2018
* Built vignette for Africa
* Initial build using the 'Hungarian' matching algorithm.
* Added a `NEWS.md` file to track changes to the package.

